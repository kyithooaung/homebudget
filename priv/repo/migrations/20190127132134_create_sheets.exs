defmodule Homebudget.Repo.Migrations.CreateSheets do
  use Ecto.Migration

  def change do
    create table(:sheets) do
      add :name, :string
      add :start_date, :date
      add :end_date, :date
      add :status, :string
      add :current_amount, :integer
      add :expense_amount, :integer

      timestamps()
    end

  end
end
