defmodule Homebudget.Repo.Migrations.CreateTransactions do
  use Ecto.Migration

  def change do
    create table(:transactions) do
      add :amount, :integer
      add :type, :string
      add :detail, :string
      add :sheet_id, references(:sheets, on_delete: :nothing)
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:transactions, [:sheet_id])
    create index(:transactions, [:user_id])
  end
end
