defmodule Homebudget.Repo.Migrations.AddDefaultValue do
  use Ecto.Migration

  def change do
  	alter table(:sheets) do
      modify :expense_amount, :integer, default: 0
    end
  end
end
