defmodule Homebudget.MasterdataTest do
  use Homebudget.DataCase

  alias Homebudget.Masterdata

  describe "sheets" do
    alias Homebudget.Masterdata.Sheet

    @valid_attrs %{current_amount: 42, end_date: ~D[2010-04-17], expense_amount: 42, name: "some name", start_date: ~D[2010-04-17], status: "some status"}
    @update_attrs %{current_amount: 43, end_date: ~D[2011-05-18], expense_amount: 43, name: "some updated name", start_date: ~D[2011-05-18], status: "some updated status"}
    @invalid_attrs %{current_amount: nil, end_date: nil, expense_amount: nil, name: nil, start_date: nil, status: nil}

    def sheet_fixture(attrs \\ %{}) do
      {:ok, sheet} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Masterdata.create_sheet()

      sheet
    end

    test "list_sheets/0 returns all sheets" do
      sheet = sheet_fixture()
      assert Masterdata.list_sheets() == [sheet]
    end

    test "get_sheet!/1 returns the sheet with given id" do
      sheet = sheet_fixture()
      assert Masterdata.get_sheet!(sheet.id) == sheet
    end

    test "create_sheet/1 with valid data creates a sheet" do
      assert {:ok, %Sheet{} = sheet} = Masterdata.create_sheet(@valid_attrs)
      assert sheet.current_amount == 42
      assert sheet.end_date == ~D[2010-04-17]
      assert sheet.expense_amount == 42
      assert sheet.name == "some name"
      assert sheet.start_date == ~D[2010-04-17]
      assert sheet.status == "some status"
    end

    test "create_sheet/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Masterdata.create_sheet(@invalid_attrs)
    end

    test "update_sheet/2 with valid data updates the sheet" do
      sheet = sheet_fixture()
      assert {:ok, %Sheet{} = sheet} = Masterdata.update_sheet(sheet, @update_attrs)
      assert sheet.current_amount == 43
      assert sheet.end_date == ~D[2011-05-18]
      assert sheet.expense_amount == 43
      assert sheet.name == "some updated name"
      assert sheet.start_date == ~D[2011-05-18]
      assert sheet.status == "some updated status"
    end

    test "update_sheet/2 with invalid data returns error changeset" do
      sheet = sheet_fixture()
      assert {:error, %Ecto.Changeset{}} = Masterdata.update_sheet(sheet, @invalid_attrs)
      assert sheet == Masterdata.get_sheet!(sheet.id)
    end

    test "delete_sheet/1 deletes the sheet" do
      sheet = sheet_fixture()
      assert {:ok, %Sheet{}} = Masterdata.delete_sheet(sheet)
      assert_raise Ecto.NoResultsError, fn -> Masterdata.get_sheet!(sheet.id) end
    end

    test "change_sheet/1 returns a sheet changeset" do
      sheet = sheet_fixture()
      assert %Ecto.Changeset{} = Masterdata.change_sheet(sheet)
    end
  end

  describe "transactions" do
    alias Homebudget.Masterdata.Transaction

    @valid_attrs %{amount: 42, detail: "some detail", type: "some type"}
    @update_attrs %{amount: 43, detail: "some updated detail", type: "some updated type"}
    @invalid_attrs %{amount: nil, detail: nil, type: nil}

    def transaction_fixture(attrs \\ %{}) do
      {:ok, transaction} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Masterdata.create_transaction()

      transaction
    end

    test "list_transactions/0 returns all transactions" do
      transaction = transaction_fixture()
      assert Masterdata.list_transactions() == [transaction]
    end

    test "get_transaction!/1 returns the transaction with given id" do
      transaction = transaction_fixture()
      assert Masterdata.get_transaction!(transaction.id) == transaction
    end

    test "create_transaction/1 with valid data creates a transaction" do
      assert {:ok, %Transaction{} = transaction} = Masterdata.create_transaction(@valid_attrs)
      assert transaction.amount == 42
      assert transaction.detail == "some detail"
      assert transaction.type == "some type"
    end

    test "create_transaction/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Masterdata.create_transaction(@invalid_attrs)
    end

    test "update_transaction/2 with valid data updates the transaction" do
      transaction = transaction_fixture()
      assert {:ok, %Transaction{} = transaction} = Masterdata.update_transaction(transaction, @update_attrs)
      assert transaction.amount == 43
      assert transaction.detail == "some updated detail"
      assert transaction.type == "some updated type"
    end

    test "update_transaction/2 with invalid data returns error changeset" do
      transaction = transaction_fixture()
      assert {:error, %Ecto.Changeset{}} = Masterdata.update_transaction(transaction, @invalid_attrs)
      assert transaction == Masterdata.get_transaction!(transaction.id)
    end

    test "delete_transaction/1 deletes the transaction" do
      transaction = transaction_fixture()
      assert {:ok, %Transaction{}} = Masterdata.delete_transaction(transaction)
      assert_raise Ecto.NoResultsError, fn -> Masterdata.get_transaction!(transaction.id) end
    end

    test "change_transaction/1 returns a transaction changeset" do
      transaction = transaction_fixture()
      assert %Ecto.Changeset{} = Masterdata.change_transaction(transaction)
    end
  end
end
