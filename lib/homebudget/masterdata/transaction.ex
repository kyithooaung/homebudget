defmodule Homebudget.Masterdata.Transaction do
  use Ecto.Schema
  import Ecto.Changeset


  schema "transactions" do
    field :amount, :integer
    field :detail, :string
    field :type, :string
    field :sheet_id, :id
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(transaction, attrs) do
    transaction
    |> cast(attrs, [:amount, :type, :detail, :user_id, :sheet_id])
    |> validate_required([:amount, :type, :detail, :user_id, :sheet_id])
  end
end
