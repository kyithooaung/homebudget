defmodule Homebudget.Masterdata.Sheet do
  use Ecto.Schema
  import Ecto.Changeset


  schema "sheets" do
    field :current_amount, :integer
    field :end_date, :date
    field :expense_amount, :integer
    field :name, :string
    field :start_date, :date
    field :status, :string

    timestamps()
  end

  @doc false
  def changeset(sheet, attrs) do
    sheet
    |> cast(attrs, [:name, :start_date, :status, :current_amount, :expense_amount, :end_date])
    |> validate_required([:name, :start_date, :status, :current_amount, :expense_amount, :end_date])
  end
end
