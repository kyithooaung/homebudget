defmodule Homebudget.Masterdata do
  @moduledoc """
  The Masterdata context.
  """

  import Ecto.Query, warn: false
  alias Homebudget.Repo

  alias Homebudget.Masterdata.Sheet
  alias Homebudget.Accounts.Account

  @doc """
  Returns the list of sheets.

  ## Examples

      iex> list_sheets()
      [%Sheet{}, ...]

  """
  def list_sheets do
    query = 
      from(
        s in Sheet,
        order_by: [desc: s.id],
      )

    Repo.all(query)
  end

  @doc """
  Gets a single sheet.

  Raises `Ecto.NoResultsError` if the Sheet does not exist.

  ## Examples

      iex> get_sheet!(123)
      %Sheet{}

      iex> get_sheet!(456)
      ** (Ecto.NoResultsError)

  """
  def get_sheet!(id), do: Repo.get!(Sheet, id)

  @doc """
  Creates a sheet.

  ## Examples

      iex> create_sheet(%{field: value})
      {:ok, %Sheet{}}

      iex> create_sheet(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_sheet(attrs \\ %{}) do
    %Sheet{}
    |> Sheet.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a sheet.

  ## Examples

      iex> update_sheet(she    query = 
      from(
        s in Sheet,
        order_by: [desc: s.id],
      )

    Repo.all(query)et, %{field: new_value})
      {:ok, %Sheet{}}

      iex> update_sheet(sheet, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_sheet(%Sheet{} = sheet, attrs) do
    sheet
    |> Sheet.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Sheet.

  ## Examples

      iex> delete_sheet(sheet)
      {:ok, %Sheet{}}

      iex> delete_sheet(sheet)
      {:error, %Ecto.Changeset{}}

  """
  def delete_sheet(%Sheet{} = sheet) do
    Repo.delete(sheet)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking sheet changes.

  ## Examples

      iex> change_sheet(sheet)
      %Ecto.Changeset{source: %Sheet{}}

  """
  def change_sheet(%Sheet{} = sheet) do
    Sheet.changeset(sheet, %{})
  end

  alias Homebudget.Masterdata.Transaction

  @doc """
  Returns the list of transactions.

  ## Examples

      iex> list_transactions()
      [%Transaction{}, ...]

  """
  def list_transactions do

    query = 
      from(
        t in Transaction,
        join: s in Sheet, 
        where: t.sheet_id == s.id,
        join: a in Account,
        where: t.user_id == a.id,
        order_by: [desc: t.id],
        select: %{
          :id         => t.id,
          :inserted_date => t.inserted_at,
          :sheet_name => s.name,
          :user_name  => a.name,
          :amount     => t.amount,
          :type       => t.type,
          :detail     => t.detail
        }
      )

    Repo.all(query)
  end

  def last_transactions(limit, type) do
    query =
      from(
        t in Transaction,
        where: t.type == ^type,
        order_by: [desc: t.inserted_at],
        limit: ^limit,
        join: s in Sheet, 
        where: t.sheet_id == s.id,
        join: a in Account,
        where: t.user_id == a.id,
        order_by: [desc: t.id],
        select: %{
          :id         => t.id,
          :inserted_date => t.inserted_at,
          :sheet_name => s.name,
          :user_name  => a.name,
          :amount     => t.amount,
          :type       => t.type,
          :detail     => t.detail
        }
      )

    Repo.all(query)
  end

  @doc """
  Gets a single transaction.

  Raises `Ecto.NoResultsError` if the Transaction does not exist.

  ## Examples

      iex> get_transaction!(123)
      %Transaction{}

      iex> get_transaction!(456)
      ** (Ecto.NoResultsError)

  """
  def get_transaction!(id), do: Repo.get!(Transaction, id)

  @doc """
  Creates a transaction.

  ## Examples

      iex> create_transaction(%{field: value})
      {:ok, %Transaction{}}

      iex> create_transaction(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_transaction(attrs \\ %{}) do
    %Transaction{}
    |> Transaction.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a transaction.

  ## Examples

      iex> update_transaction(transaction, %{field: new_value})
      {:ok, %Transaction{}}

      iex> update_transaction(transaction, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_transaction(%Transaction{} = transaction, attrs) do
    transaction
    |> Transaction.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Transaction.

  ## Examples

      iex> delete_transaction(transaction)
      {:ok, %Transaction{}}

      iex> delete_transaction(transaction)
      {:error, %Ecto.Changeset{}}

  """
  def delete_transaction(%Transaction{} = transaction) do
    Repo.delete(transaction)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking transaction changes.

  ## Examples

      iex> change_transaction(transaction)
      %Ecto.Changeset{source: %Transaction{}}

  """
  def change_transaction(%Transaction{} = transaction) do
    Transaction.changeset(transaction, %{})
  end
end
