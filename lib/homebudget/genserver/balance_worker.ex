defmodule Homebudget.Balance_worker do
  use GenServer

  def start_link do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    schedule_work()
    {:ok, state}
  end

  def handle_info(:work, state) do
    # Do the work you desire here
    IO.puts("123456")

    # This part for reschedule once more
    schedule_work()
    {:noreply, state}
  end

  # If you want to change the schedule inverval, change it here
  defp schedule_work() do
    Process.send_after(self(), :work, 1 * 30 * 1000)
  end
end
