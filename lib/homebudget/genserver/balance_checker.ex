defmodule Homebudget.UserEmail do
  import Swoosh.Email

  def welcome() do
    new()
    |> to({"Kyi Htoo Aung", "kyihtooag@gmail.com"})
    |> from({"Dr B Banner", "hulk.smash@example.com"})
    |> subject("Hello, Avengers!")
    |> html_body("<h1>Hello Kyi Htoo Aung</h1>")
    |> text_body("Hello Kyi Htoo Aung\n")
  end
end