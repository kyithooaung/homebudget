defmodule Homebudget.Accounts.Account do
  use Ecto.Schema
  import Ecto.Changeset


  schema "users" do
    field :description, :string
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(account, attrs) do
    account
    |> cast(attrs, [:name, :description])
    |> validate_required([:name, :description])
  end
end
