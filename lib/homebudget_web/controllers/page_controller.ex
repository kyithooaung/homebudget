defmodule HomebudgetWeb.PageController do
  use HomebudgetWeb, :controller

  alias Homebudget.Masterdata
  alias Homebudget.Masterdata.Sheet

  def index(conn, _params) do
  	[sheet | _] = Masterdata.list_sheets()
  	incomes = Masterdata.last_transactions(10, "income")
  	outcomes = Masterdata.last_transactions(10, "outcome")

    render(conn, "index.html", current_amount: sheet.current_amount, incomes: incomes, outcomes: outcomes)
  end
end
