defmodule HomebudgetWeb.SheetController do
  use HomebudgetWeb, :controller

  alias Homebudget.Masterdata
  alias Homebudget.Masterdata.Sheet

  def index(conn, _params) do
    sheets = Masterdata.list_sheets()
    render(conn, "index.html", sheets: sheets)
  end

  def new(conn, _params) do
    changeset = Masterdata.change_sheet(%Sheet{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"sheet" => sheet_params}) do
    case Masterdata.create_sheet(sheet_params) do
      {:ok, sheet} ->
        conn
        |> put_flash(:info, "Sheet created successfully.")
        |> redirect(to: Routes.sheet_path(conn, :show, sheet))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    sheet = Masterdata.get_sheet!(id)
    render(conn, "show.html", sheet: sheet)
  end

  def edit(conn, %{"id" => id}) do
    sheet = Masterdata.get_sheet!(id)
    changeset = Masterdata.change_sheet(sheet)
    render(conn, "edit.html", sheet: sheet, changeset: changeset)
  end

  def update(conn, %{"id" => id, "sheet" => sheet_params}) do
    sheet = Masterdata.get_sheet!(id)

    case Masterdata.update_sheet(sheet, sheet_params) do
      {:ok, sheet} ->
        conn
        |> put_flash(:info, "Sheet updated successfully.")
        |> redirect(to: Routes.sheet_path(conn, :show, sheet))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", sheet: sheet, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    sheet = Masterdata.get_sheet!(id)
    {:ok, _sheet} = Masterdata.delete_sheet(sheet)

    conn
    |> put_flash(:info, "Sheet deleted successfully.")
    |> redirect(to: Routes.sheet_path(conn, :index))
  end

  def update_status(conn, %{"id" => id}) do
    sheet = Masterdata.get_sheet!(id)
    end_date = Timex.local(Timex.now)

    params = sheet |> Map.from_struct() |> Map.put(:status, "finished")
    sheet_params = Map.put(params, :end_date, end_date)

    case Masterdata.update_sheet(sheet, sheet_params) do
      {:ok, sheet} ->
        conn
        |> put_flash(:info, "Sheet updated successfully.")
        |> redirect(to: Routes.sheet_path(conn, :show, sheet))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", sheet: sheet, changeset: changeset)
    end
  end

end
