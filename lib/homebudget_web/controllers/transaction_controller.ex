defmodule HomebudgetWeb.TransactionController do
  use HomebudgetWeb, :controller

  alias Homebudget.Masterdata
  alias Homebudget.Masterdata.Transaction
  alias Homebudget.Accounts
  alias Homebudget.Repo

  def index(conn, _params) do
    transactions = Masterdata.list_transactions() |> IO.inspect()
    render(conn, "index.html", transactions: transactions)
  end

  def new(conn, _params) do
    users = Accounts.list_users()
    sheets = Masterdata.list_sheets()
    changeset = Masterdata.change_transaction(%Transaction{})
    render(conn, "new.html", changeset: changeset, users: users, sheets: sheets)
  end

  def create(conn, %{"transaction" => transaction_params}) do
    IO.inspect(transaction_params)
    sheet = transaction_params["sheet_id"] |> Masterdata.get_sheet!()
    sheet_changeset = update_sheet(sheet, transaction_params)

    case Repo.update sheet_changeset do
      {:ok, _struct} -> case Masterdata.create_transaction(transaction_params) do
                          {:ok, transaction} ->
                            conn
                            |> put_flash(:info, "Transaction created successfully.")
                            |> redirect(to: Routes.transaction_path(conn, :show, transaction))

                          {:error, %Ecto.Changeset{} = changeset} ->
                            render(conn, "new.html", changeset: changeset)
                        end
      {:error, changeset} -> render(conn, "new.html", changeset: changeset)
    end

  end

  def update_sheet(sheet, params) do
    if params["type"] == "income" do
      update_amount = sheet.current_amount + String.to_integer(params["amount"])
      # update_expense_amount = sheet.current_amount + String.to_integer(params["amount"])
      Ecto.Changeset.change sheet, current_amount: update_amount   
    else
      update_amount = sheet.current_amount - String.to_integer(params["amount"])
      update_expense_amount = sheet.expense_amount + String.to_integer(params["amount"])
      Ecto.Changeset.change sheet, current_amount: update_amount, expense_amount: update_expense_amount
    end
  end

  def show(conn, %{"id" => id}) do
    transaction = Masterdata.get_transaction!(id)
    render(conn, "show.html", transaction: transaction)
  end

  def edit(conn, %{"id" => id}) do
    transaction = Masterdata.get_transaction!(id)
    changeset = Masterdata.change_transaction(transaction)
    render(conn, "edit.html", transaction: transaction, changeset: changeset)
  end

  def update(conn, %{"id" => id, "transaction" => transaction_params}) do
    transaction = Masterdata.get_transaction!(id)

    case Masterdata.update_transaction(transaction, transaction_params) do
      {:ok, transaction} ->
        conn
        |> put_flash(:info, "Transaction updated successfully.")
        |> redirect(to: Routes.transaction_path(conn, :show, transaction))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", transaction: transaction, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    transaction = Masterdata.get_transaction!(id)
    {:ok, _transaction} = Masterdata.delete_transaction(transaction)

    conn
    |> put_flash(:info, "Transaction deleted successfully.")
    |> redirect(to: Routes.transaction_path(conn, :index))
  end
end
