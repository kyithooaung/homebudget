defmodule HomebudgetWeb.Router do
  use HomebudgetWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", HomebudgetWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources "/sheets", SheetController
    get "/sheets/:id/updatestatus", SheetController, :update_status
    resources "/users", AccountController
    resources "/transactions", TransactionController
  end

  # Other scopes may use custom stacks.
  # scope "/api", HomebudgetWeb do
  #   pipe_through :api
  # end
end
