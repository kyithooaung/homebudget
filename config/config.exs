# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :homebudget,
  ecto_repos: [Homebudget.Repo]

# Configures the endpoint
config :homebudget, HomebudgetWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "4JGa/qhYYOrfCcBk5R5hqE7bpy9qpHhceIWyIRztnO3Yt9sEVVjq4v88/SycSWh4",
  render_errors: [view: HomebudgetWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Homebudget.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :homebudget, Homebudget.Mailer,
  adapter: Swoosh.Adapters.Sendgrid,
  api_key: "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"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
